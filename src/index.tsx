import "bootstrap/dist/css/bootstrap.min.css";
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import rootReducer from "reducers";
import App from './App/App';



ReactDOM.render(
  <React.StrictMode>
    <Provider store={createStore(rootReducer)}>
    <App />
    </Provider>
  
  </React.StrictMode>,
  document.getElementById('root')
);

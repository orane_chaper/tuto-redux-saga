import { combineReducers } from "redux";
import complete from "./completeReducer.tsx";
import incomplete from "./incompleteReducer";

export default combineReducers({
    complete,
    incomplete
});